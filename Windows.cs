using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using AHXcs;

namespace AHX
{
	public class AHXWaveOut : AHXOutput, IAHXOutput
	{
		[DllImport("kernel32.dll")]
		private static extern int GetLastError();

		[DllImport("kernel32.dll")]
		private static extern IntPtr GlobalAlloc(uint uFlags, UIntPtr dwBytes);

		[DllImport("kernel32.dll")]
		private static extern IntPtr GlobalFree(IntPtr hMem);

		[DllImport("kernel32.dll")]
		private static extern IntPtr GlobalLock(IntPtr hMem);

		[DllImport("kernel32.dll")]
		private static extern bool GlobalUnlock(IntPtr hMem);

		[DllImport("winmm.dll")]
		private static extern uint waveOutOpen(ref IntPtr hWaveOut, IntPtr uDeviceID, ref WAVEFORMATEX lpFormat, IntPtr dwCallback, IntPtr dwInstance, uint dwFlags);

		[DllImport("winmm.dll")]
		private static extern uint waveOutClose(IntPtr hwo);

		[DllImport("winmm.dll")]
		private static extern uint waveOutRestart(IntPtr hwo);

		[DllImport("winmm.dll")]
		private static extern uint waveOutPrepareHeader(IntPtr hWaveOut, ref WAVEHDR pwh, int uSize);

		[DllImport("winmm.dll")]
		private static extern uint waveOutUnprepareHeader(IntPtr hwo, ref WAVEHDR lpHdr, int cbwh);

		[DllImport("winmm.dll")]
		private static extern uint waveOutReset(IntPtr hwo);

		[DllImport("winmm.dll")]
		private static extern uint waveOutPause(IntPtr hwo);

		[DllImport("winmm.dll")]
		private static extern uint waveOutWrite(IntPtr hwo, ref WAVEHDR lpHdr, int cbwh);

		[DllImport("winmm.dll")]
		private static extern uint waveOutSetVolume(IntPtr uDeviceID, uint dwVolume);

		[StructLayout(LayoutKind.Sequential)]
		private struct WAVEFORMATEX
		{
			public ushort wFormatTag;
			public ushort nChannels;
			public uint nSamplesPerSec;
			public uint nAvgBytesPerSec;
			public ushort nBlockAlign;
			public ushort wBitsPerSample;
			public ushort cbSize;
		}

		[Flags]
		private enum WaveHdrFlags : uint
		{
			WHDR_DONE = 1,
			WHDR_PREPARED = 2,
			WHDR_BEGINLOOP = 4,
			WHDR_ENDLOOP = 8,
			WHDR_INQUEUE = 16
		}

		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
		private struct WAVEHDR
		{
			public IntPtr lpData; // pointer to locked data buffer
			public uint dwBufferLength; // length of data buffer
			public uint dwBytesRecorded; // used for input only
			public IntPtr dwUser; // for client's use
			public WaveHdrFlags dwFlags; // assorted flags (see defines)
			public uint dwLoops; // loop control counter
			public IntPtr lpNext; // PWaveHdr, reserved for driver
			public IntPtr reserved; // reserved for driver
		}

		private const int WAVE_FORMAT_PCM = 1;
		private const int WAVE_MAPPER = -1;
		private const int MMSYSERR_NOERROR = 0;

		private const int GMEM_MOVEABLE = 0x0002;
		private const int GMEM_SHARE = 0;//obsolete in win32

		private const int NO_ERROR = 0;

		private class BLOCK
		{
			public IntPtr DataHandle;
			public IntPtr Data;
			//public IntPtr HeaderHandle;
			//public IntPtr Header;
			public WAVEHDR Header;
			public int Prepared;
		}

		private BLOCK[] Blocks = new BLOCK[32];
		private int NrBlocks, Frames, BlockLen;
		private volatile int StopPlaying;

		private IntPtr hwo;
		private int Oversample;

		public AHXWaveOut()
		{
			NrBlocks = BlockLen = Frames = 0;
		}

		public void Dispose()
		{
			Free();
		}

		public int Init(int Frequency = 44100, int Bits = 16, int Frames = 2, int NrBlocks = 16, float Boost = 1.0f, int Hz = 50)
		{
			if (NrBlocks < 1 || NrBlocks > 32 || Frames < 1) return 0;

			Oversample = Playing = 0;
			this.Frames = Frames;
			this.NrBlocks = NrBlocks;
			if (base.Init(Frequency, Bits, Frames, Boost, Hz) == 0) return 0;

			BlockLen = Frequency * Bits / 8 * Frames / Hz;

			// Set up the wave format
			WAVEFORMATEX format;
			format.wFormatTag = WAVE_FORMAT_PCM;
			format.nChannels = 1;
			format.nSamplesPerSec = (uint)Frequency;
			format.wBitsPerSample = (ushort)Bits;
			format.nAvgBytesPerSec = format.nChannels * format.nSamplesPerSec * format.wBitsPerSample / 8;
			format.nBlockAlign = (ushort)(format.nChannels * format.wBitsPerSample / 8);
			format.cbSize = 0;

			// Open the playback device
			if (waveOutOpen(ref hwo, (IntPtr)WAVE_MAPPER, ref format, IntPtr.Zero, IntPtr.Zero, 0) != MMSYSERR_NOERROR) return 0;

			for (int i = 0; i < NrBlocks; i++)
			{
				Blocks[i] = new BLOCK();
				// grab memory for data
				if ((Blocks[i].DataHandle = GlobalAlloc(GMEM_MOVEABLE | GMEM_SHARE, (UIntPtr)BlockLen)) == IntPtr.Zero) return 0;
				if ((Blocks[i].Data = GlobalLock(Blocks[i].DataHandle)) == IntPtr.Zero) return 0;
				// grab memory for headers
				//if ((Blocks[i].HeaderHandle = GlobalAlloc(GMEM_MOVEABLE | GMEM_SHARE, (UIntPtr)Marshal.SizeOf<WAVEHDR>()))==IntPtr.Zero) return 0;
				//if ((Blocks[i].Header = GlobalLock(Blocks[i].HeaderHandle))==IntPtr.Zero) return 0;
				Blocks[i].Header.lpData = Blocks[i].Data;
				Blocks[i].Header.dwBufferLength = (uint)BlockLen;
				Blocks[i].Header.dwFlags = WaveHdrFlags.WHDR_DONE; // mark the block is done
				Blocks[i].Header.dwLoops = 0;
				Blocks[i].Prepared = 0;
			}

			return 1;
		}

		private int Free()
		{
			if (waveOutReset(hwo) != MMSYSERR_NOERROR) return 0;

			// wait for all blocks to finish
			for (; ; )
			{
				bool alldone = true;
				for (int i = 0; i < NrBlocks; i++)
					if ((Blocks[i].Header.dwFlags & WaveHdrFlags.WHDR_DONE) == 0)
						alldone = false;
				if (alldone) break;
				Thread.Sleep(1000 / Hz);
			}

			// unprepare all blocks
			for (int i = 0; i < NrBlocks; i++)
				if (Blocks[i].Prepared != 0)
					if (waveOutUnprepareHeader(hwo, ref Blocks[i].Header, Marshal.SizeOf<WAVEHDR>()) != MMSYSERR_NOERROR)
						return 0;

			// close wave output
			if (waveOutClose(hwo) != MMSYSERR_NOERROR) return 0;

			// kill all blocks
			for (int i = 0; i < NrBlocks; i++)
			{
				if ((!GlobalUnlock(Blocks[i].DataHandle)) && (GetLastError() != NO_ERROR)) return 0;
				if (GlobalFree(Blocks[i].DataHandle) != IntPtr.Zero) return 0;
				//if ((!GlobalUnlock(Blocks[i].HeaderHandle)) && (GetLastError() != NO_ERROR)) return 0;
				//if (GlobalFree(Blocks[i].HeaderHandle)) return 0;
			}

			return 1;
		}

		private Task t;

		public int StartBackgroundPlay()
		{
			StopPlaying = 0;
			//return _beginthread(ThreadEntry, 0, this) != -1;
			t = new Task(() => ThreadEntry(this));
			t.Start();
			return 1;
		}

		public int StopBackgroundPlay()
		{
			StopPlaying = 1;
			while (StopPlaying != 0) Thread.Sleep(Frames * (1000 / Hz) / 2);
			return 1;
		}

		public int Play(AHXPlayer Player)
		{
			if (Stop() == 0 || Player == null) return 0;
			this.Player = Player;
			Playing = 1;
			return 1;
		}

		public int Pause()
		{
			Paused = 1;
			return (int)waveOutPause(hwo);
		}

		public int Resume()
		{
			Paused = 0;
			return (waveOutRestart(hwo) == MMSYSERR_NOERROR) ? 1 : 0;
		}

		public int Stop()
		{
			Playing = 0;
			return (waveOutReset(hwo) == MMSYSERR_NOERROR) ? 1 : 0;
		}

		public int SetVolume(int Volume)
		{
			Volume = Volume * 63 / 64;
			return (waveOutSetVolume(hwo, (uint)(((Volume << 10) << 16) | (Volume << 10))) == MMSYSERR_NOERROR) ? 1 : 0;
		}

		private void ThreadEntry(object pArg)
		{
			AHXWaveOut This = (AHXWaveOut)pArg;

			// Boost the thread priority
			//SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL);

			This.EventLoop();
		}

		private void EventLoop()
		{
			// Main message loop
			while (StopPlaying == 0)
			{
				if (Playing != 0) PlayIt();
				Thread.Sleep(Frames * (1000 / Hz));
			}

			StopPlaying = 0;
			//_endthread();
		}

		private void PlayIt()
		{
			for (int i = 0; i < NrBlocks; i++)
			{
				if ((Blocks[i].Header.dwFlags & WaveHdrFlags.WHDR_DONE) == 0) continue;

				// unprepare header
				if (Blocks[i].Prepared != 0)
				{
					waveOutUnprepareHeader(hwo, ref Blocks[i].Header, Marshal.SizeOf<WAVEHDR>());
					Blocks[i].Prepared = 0;
				}

				// mix a new block (size: Frames frames = BlockLen)
				MixBuffer();

				// prepare it for waveOut
				OutputBuffer(i);
			}
		}

		private void OutputBuffer(int num)
		{
			const int LOW_CLIP16 = -0x8000;
			const int HI_CLIP16 = 0x7FFF;
			const int LOW_CLIP8 = -0x80;
			const int HI_CLIP8 = 0x7F;

			IntPtr lpChunkData = Blocks[num].Data;
			int thissample;

			if (Bits == 16)
			{
				short[] lpChunkDataW = new short[BlockLen / (Bits / 8)];
				for (int s = 0; s < BlockLen / (Bits / 8); s++)
				{
					thissample = MixingBuffer[s] << 6; // 16 bit
					lpChunkDataW[s] = (short)(thissample < LOW_CLIP16 ? LOW_CLIP16 : thissample > HI_CLIP16 ? HI_CLIP16 : thissample);
				}
				Marshal.Copy(lpChunkDataW, 0, lpChunkData, lpChunkDataW.Length);
			}
			else if (Bits == 8)
			{
				byte[] lpChunkDataB = new byte[BlockLen / (Bits / 8)];
				for (int s = 0; s < BlockLen / (Bits / 8); s++)
				{
					thissample = MixingBuffer[s] >> 2; // 8 bit
					lpChunkDataB[s] = (byte)((thissample < LOW_CLIP8 ? LOW_CLIP8 : thissample > HI_CLIP8 ? HI_CLIP8 : thissample) + 128);
				}
				Marshal.Copy(lpChunkDataB, 0, lpChunkData, lpChunkDataB.Length);
			}

			Blocks[num].Header.lpData = lpChunkData;
			Blocks[num].Header.dwBufferLength = (uint)BlockLen;
			Blocks[num].Header.dwFlags = 0;
			Blocks[num].Header.dwLoops = 0;
			waveOutPrepareHeader(hwo, ref Blocks[num].Header, Marshal.SizeOf<WAVEHDR>());
			Blocks[num].Prepared = 1;
			waveOutWrite(hwo, ref Blocks[num].Header, Marshal.SizeOf<WAVEHDR>());
		}
	}
}