using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AHX
{
	// AHXPlayer ///////////////////////////////////////////////////////////////////////////////////////////////

	public class AHXPlayer
	{
		public class AHXPListEntry
		{
			public int Note;
			public int Fixed;
			public int Waveform;
			public int[] FX = new int[2], FXParam = new int[2];
		}

		public class AHXPList
		{
			public int Speed, Length;
			public AHXPListEntry[] Entries;
		}

		public class AHXEnvelope
		{
			public int aFrames, aVolume;
			public int dFrames, dVolume;
			public int sFrames;
			public int rFrames, rVolume;
		}

		public class AHXInstrument
		{
			public string Name;
			public int Volume; // 0..64
			public int WaveLength; // 0..5 (shifts)
			public AHXEnvelope Envelope = new AHXEnvelope();
			public int FilterLowerLimit, FilterUpperLimit, FilterSpeed;
			public int SquareLowerLimit, SquareUpperLimit, SquareSpeed;
			public int VibratoDelay, VibratoDepth, VibratoSpeed;
			public int HardCutRelease, HardCutReleaseFrames;
			public AHXPList PList = new AHXPList();
		}

		public class AHXPosition
		{
			public int[] Track = new int[4];
			public int[] Transpose = new int[4];
		}

		public class AHXStep
		{
			public int Note, Instrument, FX, FXParam;
		}

		public class AHXSong
		{
			public string Name;
			public int Restart, PositionNr, TrackLength, TrackNr, InstrumentNr, SubsongNr;
			public int Revision, SpeedMultiplier;
			public AHXPosition[] Positions;
			public AHXStep[][] Tracks;
			public AHXInstrument[] Instruments;
			public int[] Subsongs;

			public AHXSong()
			{
				Restart = PositionNr = TrackLength = TrackNr = InstrumentNr = SubsongNr = 0;
				Name = null;
				Positions = null;
				Tracks = null;
				Instruments = null;
				Subsongs = null;
			}
		}

		public int PlayingTime;
		public AHXSong Song;

		public AHXWaves Waves;
		int OurWaves;
		public AHXVoice[] Voices = new AHXVoice[4];

		public int StepWaitFrames, GetNewPosition, SongEndReached, TimingValue;
		public int PatternBreak;
		public int MainVolume;
		public int Playing, Tempo;
		public int PosNr, PosJump;
		public int NoteNr, PosJumpNote;
		public Memory<sbyte>[] WaveformTab = new Memory<sbyte>[4];
		public int WNRandom;

		private static int[] VibratoTable =
		{
			0, 24, 49, 74, 97, 120, 141, 161, 180, 197, 212, 224, 235, 244, 250, 253, 255,
			253, 250, 244, 235, 224, 212, 197, 180, 161, 141, 120, 97, 74, 49, 24,
			0, -24, -49, -74, -97, -120, -141, -161, -180, -197, -212, -224, -235, -244, -250, -253, -255,
			-253, -250, -244, -235, -224, -212, -197, -180, -161, -141, -120, -97, -74, -49, -24
		};

		private static int[] PeriodTable =
		{
			0x0000, 0x0D60, 0x0CA0, 0x0BE8, 0x0B40, 0x0A98, 0x0A00, 0x0970,
			0x08E8, 0x0868, 0x07F0, 0x0780, 0x0714, 0x06B0, 0x0650, 0x05F4,
			0x05A0, 0x054C, 0x0500, 0x04B8, 0x0474, 0x0434, 0x03F8, 0x03C0,
			0x038A, 0x0358, 0x0328, 0x02FA, 0x02D0, 0x02A6, 0x0280, 0x025C,
			0x023A, 0x021A, 0x01FC, 0x01E0, 0x01C5, 0x01AC, 0x0194, 0x017D,
			0x0168, 0x0153, 0x0140, 0x012E, 0x011D, 0x010D, 0x00FE, 0x00F0,
			0x00E2, 0x00D6, 0x00CA, 0x00BE, 0x00B4, 0x00AA, 0x00A0, 0x0097,
			0x008F, 0x0087, 0x007F, 0x0078, 0x0071
		};

		public void Init()
		{
			Init(null);
		}

		public void Init(AHXWaves Waves = null)
		{
			if (Waves != null)
			{
				OurWaves = 0;
				this.Waves = Waves;
			}
			else
			{
				OurWaves = 1;
				this.Waves = new AHXWaves();
			}

			WaveformTab[0] = this.Waves.Triangle04;
			WaveformTab[1] = this.Waves.Sawtooth04;
			WaveformTab[3] = this.Waves.WhiteNoiseBig;
		}

		public void Dispose()
		{
		}

		public int LoadSong(string Filename)
		{
			var SongBuffer = File.ReadAllBytes(Filename);
			return LoadSong(SongBuffer, SongBuffer.Length);
		}

		private void strcpy(out string name, byte[] sb, int ptr)
		{
			var s = new StringBuilder();
			while (sb[ptr] != 0)
				s.Append((char)sb[ptr++]);
			name = s.ToString();
		}

		private int strlen(byte[] sb, int ptr)
		{
			int sptr = ptr;
			while (sb[ptr] != 0) ptr++;
			return ptr - sptr;
		}

		public int LoadSong(byte[] Buffer, int Len)
		{
			byte[] SongBuffer = Buffer;
			int SBPtr = 14;
			int SongLength = Len;
			if (SongLength < 14 || SongLength == 65536) return 0;

			if (SongBuffer[0] != 'T' && SongBuffer[1] != 'H' && SongBuffer[2] != 'X') return 0;
			Song = new AHXSong();
			Song.Revision = SongBuffer[3];
			if (Song.Revision > 1) return 0;

			// Header ////////////////////////////////////////////
			// Songname
			int NamePtr = (SongBuffer[4] << 8) | SongBuffer[5];
			strcpy(out Song.Name, SongBuffer, NamePtr);
			NamePtr += strlen(SongBuffer, NamePtr) + 1;
			Song.SpeedMultiplier = ((SongBuffer[6] >> 5) & 3) + 1;

			Song.PositionNr = ((SongBuffer[6] & 0xf) << 8) | SongBuffer[7];
			Song.Restart = (SongBuffer[8] << 8) | SongBuffer[9];
			Song.TrackLength = SongBuffer[10];
			Song.TrackNr = SongBuffer[11];
			Song.InstrumentNr = SongBuffer[12];
			Song.SubsongNr = SongBuffer[13];

			// Subsongs //////////////////////////////////////////
			Song.Subsongs = new int[Song.SubsongNr];
			for (int i = 0; i < Song.SubsongNr; i++)
			{
				if (SBPtr >= SongLength) return 0;
				Song.Subsongs[i] = (SongBuffer[SBPtr] << 8) | SongBuffer[SBPtr + 1];
				SBPtr += 2;
			}

			// Position List /////////////////////////////////////
			Song.Positions = new AHXPosition[Song.PositionNr];
			for (int i = 0; i < Song.PositionNr; i++)
			{
				Song.Positions[i] = new AHXPosition();
				for (int j = 0; j < 4; j++)
				{
					if (SBPtr > SongLength) return 0;
					Song.Positions[i].Track[j] = SongBuffer[SBPtr++];
					Song.Positions[i].Transpose[j] = (sbyte)SongBuffer[SBPtr++];
				}
			}

			// Tracks ////////////////////////////////////////////
			int MaxTrack = Song.TrackNr;
			Song.Tracks = new AHXStep[MaxTrack + 1][];

			for (int i = 0; i < MaxTrack + 1; i++)
			{
				Song.Tracks[i] = new AHXStep[Song.TrackLength];
				if ((SongBuffer[6] & 0x80) == 0x80 && i == 0)
				{
					//will already by blank
					//memset(Song.Tracks[i], 0, Song.TrackLength * sizeof(AHXStep));
					for (int j = 0; j < Song.TrackLength; j++)
						Song.Tracks[i][j] = new AHXStep();
					continue;
				}

				for (int j = 0; j < Song.TrackLength; j++)
				{
					Song.Tracks[i][j] = new AHXStep();
					if (SBPtr >= SongLength) return 0;
					Song.Tracks[i][j].Note = (SongBuffer[SBPtr] >> 2) & 0x3f;
					Song.Tracks[i][j].Instrument = ((SongBuffer[SBPtr] & 0x3) << 4) | (SongBuffer[SBPtr + 1] >> 4);
					Song.Tracks[i][j].FX = SongBuffer[SBPtr + 1] & 0xf;
					Song.Tracks[i][j].FXParam = SongBuffer[SBPtr + 2];
					SBPtr += 3;
				}
			}

			// Instruments ///////////////////////////////////////
			Song.Instruments = new AHXInstrument[Song.InstrumentNr + 1];
			for (int i = 1; i < Song.InstrumentNr + 1; i++)
			{
				Song.Instruments[i] = new AHXInstrument();
				strcpy(out Song.Instruments[i].Name, SongBuffer, NamePtr);
				NamePtr += strlen(SongBuffer, NamePtr) + 1;

				if (SBPtr >= SongLength) return 0;
				Song.Instruments[i].Volume = SongBuffer[SBPtr + 0];
				Song.Instruments[i].FilterSpeed = ((SongBuffer[SBPtr + 1] >> 3) & 0x1f) | ((SongBuffer[SBPtr + 12] >> 2) & 0x20);
				Song.Instruments[i].WaveLength = SongBuffer[SBPtr + 1] & 0x7;
				Song.Instruments[i].Envelope.aFrames = SongBuffer[SBPtr + 2];
				Song.Instruments[i].Envelope.aVolume = SongBuffer[SBPtr + 3];
				Song.Instruments[i].Envelope.dFrames = SongBuffer[SBPtr + 4]; //4
				Song.Instruments[i].Envelope.dVolume = SongBuffer[SBPtr + 5];
				Song.Instruments[i].Envelope.sFrames = SongBuffer[SBPtr + 6];
				Song.Instruments[i].Envelope.rFrames = SongBuffer[SBPtr + 7]; //7
				Song.Instruments[i].Envelope.rVolume = SongBuffer[SBPtr + 8];
				Song.Instruments[i].FilterLowerLimit = SongBuffer[SBPtr + 12] & 0x7f;
				Song.Instruments[i].VibratoDelay = SongBuffer[SBPtr + 13]; //13
				Song.Instruments[i].HardCutReleaseFrames = (SongBuffer[SBPtr + 14] >> 4) & 7;
				Song.Instruments[i].HardCutRelease = (SongBuffer[SBPtr + 14] & 0x80) != 0 ? 1 : 0;
				Song.Instruments[i].VibratoDepth = SongBuffer[SBPtr + 14] & 0xf; //14
				Song.Instruments[i].VibratoSpeed = SongBuffer[SBPtr + 15];
				Song.Instruments[i].SquareLowerLimit = SongBuffer[SBPtr + 16];
				Song.Instruments[i].SquareUpperLimit = SongBuffer[SBPtr + 17]; //17
				Song.Instruments[i].SquareSpeed = SongBuffer[SBPtr + 18];
				Song.Instruments[i].FilterUpperLimit = SongBuffer[SBPtr + 19] & 0x3f; //19
				Song.Instruments[i].PList.Speed = SongBuffer[SBPtr + 20];
				Song.Instruments[i].PList.Length = SongBuffer[SBPtr + 21];
				SBPtr += 22;
				Song.Instruments[i].PList.Entries = new AHXPListEntry[Song.Instruments[i].PList.Length];
				for (int j = 0; j < Song.Instruments[i].PList.Length; j++)
				{
					if (SBPtr >= SongLength) return 0;
					Song.Instruments[i].PList.Entries[j] = new AHXPListEntry();
					Song.Instruments[i].PList.Entries[j].FX[1] = (SongBuffer[SBPtr + 0] >> 5) & 7;
					Song.Instruments[i].PList.Entries[j].FX[0] = (SongBuffer[SBPtr + 0] >> 2) & 7;
					Song.Instruments[i].PList.Entries[j].Waveform = ((SongBuffer[SBPtr + 0] << 1) & 6) | (SongBuffer[SBPtr + 1] >> 7);
					Song.Instruments[i].PList.Entries[j].Fixed = (SongBuffer[SBPtr + 1] >> 6) & 1;
					Song.Instruments[i].PList.Entries[j].Note = SongBuffer[SBPtr + 1] & 0x3f;
					Song.Instruments[i].PList.Entries[j].FXParam[0] = SongBuffer[SBPtr + 2];
					Song.Instruments[i].PList.Entries[j].FXParam[1] = SongBuffer[SBPtr + 3];
					SBPtr += 4;
				}
			}

			return 1;
		}

		public int InitSubsong(int Nr)
		{
			if (Nr > Song.SubsongNr) return 0;

			if (Nr == 0) PosNr = 0;
			else PosNr = Song.Subsongs[Nr - 1];

			PosJump = 0;
			PatternBreak = 0;
			MainVolume = 0x40;
			Playing = 1;
			NoteNr = PosJumpNote = 0;
			Tempo = 6;
			StepWaitFrames = 0;
			GetNewPosition = 1;
			SongEndReached = 0;
			TimingValue = PlayingTime = 0;

			for (int v = 0; v < 4; v++)
			{
				Voices[v] = new AHXVoice();
				Voices[v].Init();
			}

			return 1;
		}

		public void PlayIRQ()
		{
			if (StepWaitFrames <= 0)
			{
				if (GetNewPosition != 0)
				{
					int NextPos = (PosNr + 1 == Song.PositionNr) ? 0 : (PosNr + 1);
					for (int i = 0; i < 4; i++)
					{
						Voices[i].Track = Song.Positions[PosNr].Track[i];
						Voices[i].Transpose = Song.Positions[PosNr].Transpose[i];
						Voices[i].NextTrack = Song.Positions[NextPos].Track[i];
						Voices[i].NextTranspose = Song.Positions[NextPos].Transpose[i];
					}

					GetNewPosition = 0;
				}

				for (int i = 0; i < 4; i++) ProcessStep(i);
				StepWaitFrames = Tempo;
			}

			//DoFrameStuff
			for (int i = 0; i < 4; i++) ProcessFrame(i);
			PlayingTime++;
			if (Tempo > 0 && --StepWaitFrames <= 0)
			{
				if (PatternBreak == 0)
				{
					NoteNr++;
					if (NoteNr >= Song.TrackLength)
					{
						PosJump = PosNr + 1;
						PosJumpNote = 0;
						PatternBreak = 1;
					}
				}

				if (PatternBreak != 0)
				{
					PatternBreak = 0;
					NoteNr = PosJumpNote;
					PosJumpNote = 0;
					PosNr = PosJump;
					PosJump = 0;
					if (PosNr == Song.PositionNr)
					{
						SongEndReached = 1;
						PosNr = Song.Restart;
					}

					GetNewPosition = 1;
				}
			}

			//RemainPosition
			for (int a = 0; a < 4; a++) SetAudio(a);
		}

		public void NextPosition()
		{
			PosNr++;
			if (PosNr == Song.PositionNr) PosNr = 0;
			StepWaitFrames = 0;
			GetNewPosition = 1;
		}

		public void PrevPosition()
		{
			PosNr--;
			if (PosNr < 0) PosNr = 0;
			StepWaitFrames = 0;
			GetNewPosition = 1;
		}

		public void ProcessStep(int v)
		{
			if (Voices[v].TrackOn == 0) return;
			Voices[v].VolumeSlideUp = Voices[v].VolumeSlideDown = 0;

			int Note = Song.Tracks[Song.Positions[PosNr].Track[v]][NoteNr].Note;
			int Instrument = Song.Tracks[Song.Positions[PosNr].Track[v]][NoteNr].Instrument;
			int FX = Song.Tracks[Song.Positions[PosNr].Track[v]][NoteNr].FX;
			int FXParam = Song.Tracks[Song.Positions[PosNr].Track[v]][NoteNr].FXParam;

			switch (FX)
			{
				case 0x0: // Position Jump HI
					if ((FXParam & 0xf) > 0 && (FXParam & 0xf) <= 9)
						PosJump = FXParam & 0xf;
					break;
				case 0x5: // Volume Slide + Tone Portamento
				case 0xa: // Volume Slide
					Voices[v].VolumeSlideDown = FXParam & 0x0f;
					Voices[v].VolumeSlideUp = FXParam >> 4;
					break;
				case 0xb: // Position Jump
					PosJump = PosJump * 100 + (FXParam & 0x0f) + (FXParam >> 4) * 10;
					PatternBreak = 1;
					break;
				case 0xd: // Patternbreak
					PosJump = PosNr + 1;
					PosJumpNote = (FXParam & 0x0f) + (FXParam >> 4) * 10;
					if (PosJumpNote > Song.TrackLength) PosJumpNote = 0;
					PatternBreak = 1;
					break;
				case 0xe: // Enhanced commands
					switch (FXParam >> 4)
					{
						case 0xc: // Note Cut
							if ((FXParam & 0x0f) < Tempo)
							{
								Voices[v].NoteCutWait = FXParam & 0x0f;
								if (Voices[v].NoteCutWait != 0)
								{
									Voices[v].NoteCutOn = 1;
									Voices[v].HardCutRelease = 0;
								}
							}

							break;
						case 0xd: // Note Delay
							if (Voices[v].NoteDelayOn != 0)
							{
								Voices[v].NoteDelayOn = 0;
							}
							else
							{
								if ((FXParam & 0x0f) < Tempo)
								{
									Voices[v].NoteDelayWait = FXParam & 0x0f;
									if (Voices[v].NoteDelayWait != 0)
									{
										Voices[v].NoteDelayOn = 1;
										return;
									}
								}
							}

							break;
					}

					break;
				case 0xf: // Speed
					Tempo = FXParam;
					break;
			}

			if (Instrument != 0)
			{
				Voices[v].PerfSubVolume = 0x40;
				Voices[v].PeriodSlideSpeed = Voices[v].PeriodSlidePeriod = Voices[v].PeriodSlideLimit = 0;
				Voices[v].ADSRVolume = 0;
				Voices[v].Instrument = Song.Instruments[Instrument];
				Voices[v].CalcADSR();
				//InitOnInstrument
				Voices[v].WaveLength = Voices[v].Instrument.WaveLength;
				Voices[v].NoteMaxVolume = Voices[v].Instrument.Volume;
				//InitVibrato
				Voices[v].VibratoCurrent = 0;
				Voices[v].VibratoDelay = Voices[v].Instrument.VibratoDelay;
				Voices[v].VibratoDepth = Voices[v].Instrument.VibratoDepth;
				Voices[v].VibratoSpeed = Voices[v].Instrument.VibratoSpeed;
				Voices[v].VibratoPeriod = 0;
				//InitHardCut
				Voices[v].HardCutRelease = Voices[v].Instrument.HardCutRelease;
				Voices[v].HardCut = Voices[v].Instrument.HardCutReleaseFrames;
				//InitSquare
				Voices[v].IgnoreSquare = Voices[v].SquareSlidingIn = 0;
				Voices[v].SquareWait = Voices[v].SquareOn = 0;
				int SquareLower = Voices[v].Instrument.SquareLowerLimit >> (5 - Voices[v].WaveLength);
				int SquareUpper = Voices[v].Instrument.SquareUpperLimit >> (5 - Voices[v].WaveLength);
				if (SquareUpper < SquareLower)
				{
					int t = SquareUpper;
					SquareUpper = SquareLower;
					SquareLower = t;
				}

				Voices[v].SquareUpperLimit = SquareUpper;
				Voices[v].SquareLowerLimit = SquareLower;
				//InitFilter
				Voices[v].IgnoreFilter = Voices[v].FilterWait = Voices[v].FilterOn = 0;
				Voices[v].FilterSlidingIn = 0;
				int d6 = Voices[v].Instrument.FilterSpeed;
				int d3 = Voices[v].Instrument.FilterLowerLimit;
				int d4 = Voices[v].Instrument.FilterUpperLimit;
				if ((d3 & 0x80) != 0) d6 |= 0x20;
				if ((d4 & 0x80) != 0) d6 |= 0x40;
				Voices[v].FilterSpeed = d6;
				d3 &= ~0x80;
				d4 &= ~0x80;
				if (d3 > d4)
				{
					int t = d3;
					d3 = d4;
					d4 = t;
				}

				Voices[v].FilterUpperLimit = d4;
				Voices[v].FilterLowerLimit = d3;
				Voices[v].FilterPos = 32;
				//Init PerfList
				Voices[v].PerfWait = Voices[v].PerfCurrent = 0;
				Voices[v].PerfSpeed = Voices[v].Instrument.PList.Speed;
				Voices[v].PerfList = Voices[v].Instrument.PList;
			}

			//NoInstrument
			Voices[v].PeriodSlideOn = 0;

			switch (FX)
			{
				case 0x4: // Override filter
					break;
				case 0x9: // Set Squarewave-Offset
					Voices[v].SquarePos = FXParam >> (5 - Voices[v].WaveLength);
					Voices[v].PlantSquare = 1;
					Voices[v].IgnoreSquare = 1;
					break;
				case 0x5: // Tone Portamento + Volume Slide
				case 0x3: // Tone Portamento (Period Slide Up/Down w/ Limit)
					if (FXParam != 0) Voices[v].PeriodSlideSpeed = FXParam;
					if (Note != 0)
					{
						int Neue = PeriodTable[Note];
						int Alte = PeriodTable[Voices[v].TrackPeriod];
						Alte -= Neue;
						Neue = Alte + Voices[v].PeriodSlidePeriod;
						if (Neue != 0) Voices[v].PeriodSlideLimit = -Alte;
					}

					Voices[v].PeriodSlideOn = 1;
					Voices[v].PeriodSlideWithLimit = 1;
					goto NoNote;
			}

			// Note anschlagen
			if (Note != 0)
			{
				Voices[v].TrackPeriod = Note;
				Voices[v].PlantPeriod = 1;
			}

			NoNote:
			switch (FX)
			{
				case 0x1: // Portamento up (Period slide down)
					Voices[v].PeriodSlideSpeed = -FXParam;
					Voices[v].PeriodSlideOn = 1;
					Voices[v].PeriodSlideWithLimit = 0;
					break;
				case 0x2: // Portamento down (Period slide up)
					Voices[v].PeriodSlideSpeed = FXParam;
					Voices[v].PeriodSlideOn = 1;
					Voices[v].PeriodSlideWithLimit = 0;
					break;
				case 0xc: // Volume
					if (FXParam <= 0x40)
						Voices[v].NoteMaxVolume = FXParam;
					else
					{
						FXParam -= 0x50;
						if (FXParam <= 0x40)
							for (int i = 0; i < 4; i++)
								Voices[i].TrackMasterVolume = FXParam;
						else
						{
							FXParam -= 0xa0 - 0x50;
							if (FXParam <= 0x40)
								Voices[v].TrackMasterVolume = FXParam;
						}
					}

					break;
				case 0xe: // Enhanced commands
					switch (FXParam >> 4)
					{
						case 0x1: // Fineslide up (Period fineslide down)
							Voices[v].PeriodSlidePeriod = -(FXParam & 0x0f);
							Voices[v].PlantPeriod = 1;
							break;
						case 0x2: // Fineslide down (Period fineslide up)
							Voices[v].PeriodSlidePeriod = FXParam & 0x0f;
							Voices[v].PlantPeriod = 1;
							break;
						case 0x4: // Vibrato control
							Voices[v].VibratoDepth = FXParam & 0x0f;
							break;
						case 0xa: // Finevolume up
							Voices[v].NoteMaxVolume += FXParam & 0x0f;
							if (Voices[v].NoteMaxVolume > 0x40) Voices[v].NoteMaxVolume = 0x40;
							break;
						case 0xb: // Finevolume down
							Voices[v].NoteMaxVolume -= FXParam & 0x0f;
							if (Voices[v].NoteMaxVolume < 0) Voices[v].NoteMaxVolume = 0;
							break;
					}

					break;
			}
		}

		public void ProcessFrame(int v)
		{
			if (Voices[v].TrackOn == 0) return;

			if (Voices[v].NoteDelayOn != 0)
			{
				if (Voices[v].NoteDelayWait <= 0) ProcessStep(v);
				else Voices[v].NoteDelayWait--;
			}

			if (Voices[v].HardCut != 0)
			{
				int NextInstrument;
				if (NoteNr + 1 < Song.TrackLength) NextInstrument = Song.Tracks[Voices[v].Track][NoteNr + 1].Instrument;
				else NextInstrument = Song.Tracks[Voices[v].NextTrack][0].Instrument;
				if (NextInstrument != 0)
				{
					int d1 = Tempo - Voices[v].HardCut;
					if (d1 < 0) d1 = 0;
					if (Voices[v].NoteCutOn == 0)
					{
						Voices[v].NoteCutOn = 1;
						Voices[v].NoteCutWait = d1;
						Voices[v].HardCutReleaseF = -(d1 - Tempo);
					}
					else Voices[v].HardCut = 0;
				}
			}

			if (Voices[v].NoteCutOn != 0)
			{
				if (Voices[v].NoteCutWait <= 0)
				{
					Voices[v].NoteCutOn = 0;
					if (Voices[v].HardCutRelease != 0)
					{
						Voices[v].ADSR.rVolume = -(Voices[v].ADSRVolume - (Voices[v].Instrument.Envelope.rVolume << 8)) / Voices[v].HardCutReleaseF;
						Voices[v].ADSR.rFrames = Voices[v].HardCutReleaseF;
						Voices[v].ADSR.aFrames = Voices[v].ADSR.dFrames = Voices[v].ADSR.sFrames = 0;
					}
					else Voices[v].NoteMaxVolume = 0;
				}
				else Voices[v].NoteCutWait--;
			}

			//adsrEnvelope
			if (Voices[v].ADSR.aFrames != 0)
			{
				Voices[v].ADSRVolume += Voices[v].ADSR.aVolume; // Delta
				if (--Voices[v].ADSR.aFrames <= 0) Voices[v].ADSRVolume = Voices[v].Instrument.Envelope.aVolume << 8;
			}
			else if (Voices[v].ADSR.dFrames != 0)
			{
				Voices[v].ADSRVolume += Voices[v].ADSR.dVolume; // Delta
				if (--Voices[v].ADSR.dFrames <= 0) Voices[v].ADSRVolume = Voices[v].Instrument.Envelope.dVolume << 8;
			}
			else if (Voices[v].ADSR.sFrames != 0)
			{
				Voices[v].ADSR.sFrames--;
			}
			else if (Voices[v].ADSR.rFrames != 0)
			{
				Voices[v].ADSRVolume += Voices[v].ADSR.rVolume; // Delta
				if (--Voices[v].ADSR.rFrames <= 0) Voices[v].ADSRVolume = Voices[v].Instrument.Envelope.rVolume << 8;
			}

			//VolumeSlide
			Voices[v].NoteMaxVolume = Voices[v].NoteMaxVolume + Voices[v].VolumeSlideUp - Voices[v].VolumeSlideDown;
			if (Voices[v].NoteMaxVolume < 0) Voices[v].NoteMaxVolume = 0;
			if (Voices[v].NoteMaxVolume > 0x40) Voices[v].NoteMaxVolume = 0x40;
			//Portamento
			if (Voices[v].PeriodSlideOn != 0)
			{
				if (Voices[v].PeriodSlideWithLimit != 0)
				{
					int d0 = Voices[v].PeriodSlidePeriod - Voices[v].PeriodSlideLimit;
					int d2 = Voices[v].PeriodSlideSpeed;
					if (d0 > 0) d2 = -d2;
					if (d0 != 0)
					{
						int d3 = (d0 + d2) ^ d0;
						if (d3 >= 0) d0 = Voices[v].PeriodSlidePeriod + d2;
						else d0 = Voices[v].PeriodSlideLimit;
						Voices[v].PeriodSlidePeriod = d0;
						Voices[v].PlantPeriod = 1;
					}
				}
				else
				{
					Voices[v].PeriodSlidePeriod += Voices[v].PeriodSlideSpeed;
					Voices[v].PlantPeriod = 1;
				}
			}

			//Vibrato
			if (Voices[v].VibratoDepth != 0)
			{
				if (Voices[v].VibratoDelay <= 0)
				{
					Voices[v].VibratoPeriod = (VibratoTable[Voices[v].VibratoCurrent] * Voices[v].VibratoDepth) >> 7;
					Voices[v].PlantPeriod = 1;
					Voices[v].VibratoCurrent = (Voices[v].VibratoCurrent + Voices[v].VibratoSpeed) & 0x3f;
				}
				else Voices[v].VibratoDelay--;
			}

			//PList
			if (Voices[v].Instrument != null && Voices[v].PerfCurrent < Voices[v].Instrument.PList.Length)
			{
				if (--Voices[v].PerfWait <= 0)
				{
					int Cur = Voices[v].PerfCurrent++;
					Voices[v].PerfWait = Voices[v].PerfSpeed;
					if (Voices[v].PerfList.Entries[Cur].Waveform != 0)
					{
						Voices[v].Waveform = Voices[v].PerfList.Entries[Cur].Waveform - 1;
						Voices[v].NewWaveform = 1;
						Voices[v].PeriodPerfSlideSpeed = Voices[v].PeriodPerfSlidePeriod = 0;
					}

					//Holdwave
					Voices[v].PeriodPerfSlideOn = 0;
					for (int i = 0; i < 2; i++) PListCommandParse(v, Voices[v].PerfList.Entries[Cur].FX[i], Voices[v].PerfList.Entries[Cur].FXParam[i]);
					//GetNote
					if (Voices[v].PerfList.Entries[Cur].Note != 0)
					{
						Voices[v].InstrPeriod = Voices[v].PerfList.Entries[Cur].Note;
						Voices[v].PlantPeriod = 1;
						Voices[v].FixedNote = Voices[v].PerfList.Entries[Cur].Fixed;
					}
				}
			}
			else
			{
				if (Voices[v].PerfWait != 0) Voices[v].PerfWait--;
				else Voices[v].PeriodPerfSlideSpeed = 0;
			}

			//PerfPortamento
			if (Voices[v].PeriodPerfSlideOn != 0)
			{
				Voices[v].PeriodPerfSlidePeriod -= Voices[v].PeriodPerfSlideSpeed;
				if (Voices[v].PeriodPerfSlidePeriod != 0) Voices[v].PlantPeriod = 1;
			}

			if (Voices[v].Waveform == 3 - 1 && Voices[v].SquareOn != 0)
			{
				if (--Voices[v].SquareWait <= 0)
				{
					int d1 = Voices[v].SquareLowerLimit;
					int d2 = Voices[v].SquareUpperLimit;
					int d3 = Voices[v].SquarePos;
					if (Voices[v].SquareInit != 0)
					{
						Voices[v].SquareInit = 0;
						if (d3 <= d1)
						{
							Voices[v].SquareSlidingIn = 1;
							Voices[v].SquareSign = 1;
						}
						else if (d3 >= d2)
						{
							Voices[v].SquareSlidingIn = 1;
							Voices[v].SquareSign = -1;
						}
					}

					//NoSquareInit
					if (d1 == d3 || d2 == d3)
					{
						if (Voices[v].SquareSlidingIn != 0)
						{
							Voices[v].SquareSlidingIn = 0;
						}
						else
						{
							Voices[v].SquareSign = -Voices[v].SquareSign;
						}
					}

					d3 += Voices[v].SquareSign;
					Voices[v].SquarePos = d3;
					Voices[v].PlantSquare = 1;
					Voices[v].SquareWait = Voices[v].Instrument.SquareSpeed;
				}
			}

			if (Voices[v].FilterOn != 0 && --Voices[v].FilterWait <= 0)
			{
				int d1 = Voices[v].FilterLowerLimit;
				int d2 = Voices[v].FilterUpperLimit;
				int d3 = Voices[v].FilterPos;
				if (Voices[v].FilterInit != 0)
				{
					Voices[v].FilterInit = 0;
					if (d3 <= d1)
					{
						Voices[v].FilterSlidingIn = 1;
						Voices[v].FilterSign = 1;
					}
					else if (d3 >= d2)
					{
						Voices[v].FilterSlidingIn = 1;
						Voices[v].FilterSign = -1;
					}
				}

				//NoFilterInit
				int FMax = (Voices[v].FilterSpeed < 3) ? (5 - Voices[v].FilterSpeed) : 1;
				for (int i = 0; i < FMax; i++)
				{
					if (d1 == d3 || d2 == d3)
					{
						if (Voices[v].FilterSlidingIn != 0)
						{
							Voices[v].FilterSlidingIn = 0;
						}
						else
						{
							Voices[v].FilterSign = -Voices[v].FilterSign;
						}
					}

					d3 += Voices[v].FilterSign;
				}

				Voices[v].FilterPos = d3;
				Voices[v].NewWaveform = 1;
				Voices[v].FilterWait = Voices[v].FilterSpeed - 3;
				if (Voices[v].FilterWait < 1) Voices[v].FilterWait = 1;
			}

			if (Voices[v].Waveform == 3 - 1 || Voices[v].PlantSquare != 0)
			{
				//CalcSquare
				//sbyte[] SquarePtr = Waves.Squares[(Voices[v].FilterPos - 0x20) * (0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 0x280 * 3)];
				int SquarePtr = AHXWaves.offSquares + (Voices[v].FilterPos - 0x20) * (0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 0x280 * 3);
				int X = Voices[v].SquarePos << (5 - Voices[v].WaveLength);
				if (X > 0x20)
				{
					X = 0x40 - X;
					Voices[v].SquareReverse = 1;
				}

				//OkDownSquare
				if (--X != 0) SquarePtr += X << 7;
				int Delta = 32 >> Voices[v].WaveLength;
				WaveformTab[2] = Voices[v].SquareTempBuffer;
				for (int i = 0; i < (1 << Voices[v].WaveLength) * 4; i++)
				{
					Voices[v].SquareTempBuffer[i] = Waves.WaveBuffer[SquarePtr];
					SquarePtr += Delta;
				}

				Voices[v].NewWaveform = 1;
				Voices[v].Waveform = 3 - 1;
				Voices[v].PlantSquare = 0;
			}

			if (Voices[v].Waveform == 4 - 1) Voices[v].NewWaveform = 1;

			if (Voices[v].NewWaveform != 0)
			{
				if (Voices[v].Waveform == 2)
				{
					Voices[v].AudioSource = WaveformTab[Voices[v].Waveform];
				}
				else
				{
					//sbyte[] AudioSource = WaveformTab[Voices[v].Waveform];
					int AudioSource = Waves.WaveToOffset(WaveformTab[Voices[v].Waveform]);
					if (Voices[v].Waveform != 3 - 1)
					{
						AudioSource += (Voices[v].FilterPos - 0x20) * (0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 0x280 * 3);
					}

					if (Voices[v].Waveform < 3 - 1)
					{
						//GetWLWaveformlor2
						int[] Offsets = {0x00, 0x04, 0x04 + 0x08, 0x04 + 0x08 + 0x10, 0x04 + 0x08 + 0x10 + 0x20, 0x04 + 0x08 + 0x10 + 0x20 + 0x40};
						AudioSource += Offsets[Voices[v].WaveLength];
					}

					if (Voices[v].Waveform == 4 - 1)
					{
						//AddRandomMoving
						AudioSource += (WNRandom & (2 * 0x280 - 1)) & ~1;
						//GoOnRandom
						WNRandom += 2239384;
						WNRandom = ((((WNRandom >> 8) | (WNRandom << 24)) + 782323) ^ 75) - 6735;
					}

					Voices[v].AudioSource = Waves.OffsetToWave(AudioSource);
				}
			}

			//StillHoldWaveform
			//AudioInitPeriod
			Voices[v].AudioPeriod = Voices[v].InstrPeriod;
			if (Voices[v].FixedNote == 0) Voices[v].AudioPeriod += Voices[v].Transpose + Voices[v].TrackPeriod - 1;
			if (Voices[v].AudioPeriod > 5 * 12) Voices[v].AudioPeriod = 5 * 12;
			if (Voices[v].AudioPeriod < 0) Voices[v].AudioPeriod = 0;
			Voices[v].AudioPeriod = PeriodTable[Voices[v].AudioPeriod];
			if (Voices[v].FixedNote == 0) Voices[v].AudioPeriod += Voices[v].PeriodSlidePeriod;
			Voices[v].AudioPeriod += Voices[v].PeriodPerfSlidePeriod + Voices[v].VibratoPeriod;
			if (Voices[v].AudioPeriod > 0x0d60) Voices[v].AudioPeriod = 0x0d60;
			if (Voices[v].AudioPeriod < 0x0071) Voices[v].AudioPeriod = 0x0071;
			//AudioInitVolume
			Voices[v].AudioVolume = ((((((((Voices[v].ADSRVolume >> 8) * Voices[v].NoteMaxVolume) >> 6) * Voices[v].PerfSubVolume) >> 6) * Voices[v].TrackMasterVolume) >> 6) * MainVolume) >> 6;
		}

		public void SetAudio(int v)
		{
			if (Voices[v].TrackOn == 0)
			{
				Voices[v].VoiceVolume = 0;
				return;
			}

			Voices[v].VoiceVolume = Voices[v].AudioVolume;
			if (Voices[v].PlantPeriod != 0)
			{
				Voices[v].PlantPeriod = 0;
				Voices[v].VoicePeriod = Voices[v].AudioPeriod;
			}

			if (Voices[v].NewWaveform != 0)
			{
				if (Voices[v].Waveform == 4 - 1)
				{
					var src = Voices[v].AudioSource.Span;
					for (int j = 0; j < 0x280; j++)
						Voices[v].VoiceBuffer[j] = src[j];
				}
				else
				{
					int WaveLoops = (1 << (5 - Voices[v].WaveLength)) * 5;
					for (int i = 0; i < WaveLoops; i++)
					{
						var src = Voices[v].AudioSource.Span;
						int size = 4 * (1 << Voices[v].WaveLength);
						for (int j = 0; j < size; j++)
							Voices[v].VoiceBuffer[i * size + j] = src[j];
					}
				}

				Voices[v].VoiceBuffer[0x280] = Voices[v].VoiceBuffer[0];
			}
		}

		public void PListCommandParse(int v, int FX, int FXParam)
		{
			switch (FX)
			{
				case 0:
					if (Song.Revision > 0 && FXParam != 0)
					{
						if (Voices[v].IgnoreFilter != 0)
						{
							Voices[v].FilterPos = Voices[v].IgnoreFilter;
							Voices[v].IgnoreFilter = 0;
						}
						else Voices[v].FilterPos = FXParam;

						Voices[v].NewWaveform = 1;
					}

					break;
				case 1:
					Voices[v].PeriodPerfSlideSpeed = FXParam;
					Voices[v].PeriodPerfSlideOn = 1;
					break;
				case 2:
					Voices[v].PeriodPerfSlideSpeed = -FXParam;
					Voices[v].PeriodPerfSlideOn = 1;
					break;
				case 3: // Init Square Modulation
					if (Voices[v].IgnoreSquare == 0)
					{
						Voices[v].SquarePos = FXParam >> (5 - Voices[v].WaveLength);
					}
					else Voices[v].IgnoreSquare = 0;

					break;
				case 4: // Start/Stop Modulation
					if (Song.Revision == 0 || FXParam == 0)
					{
						Voices[v].SquareInit = (Voices[v].SquareOn ^= 1);
						Voices[v].SquareSign = 1;
					}
					else
					{
						if ((FXParam & 0x0f) != 0)
						{
							Voices[v].SquareInit = (Voices[v].SquareOn ^= 1);
							Voices[v].SquareSign = 1;
							if ((FXParam & 0x0f) == 0x0f) Voices[v].SquareSign = -1;
						}

						if ((FXParam & 0xf0) != 0)
						{
							Voices[v].FilterInit = (Voices[v].FilterOn ^= 1);
							Voices[v].FilterSign = 1;
							if ((FXParam & 0xf0) == 0xf0) Voices[v].FilterSign = -1;
						}
					}

					break;
				case 5: // Jump to Step [xx]
					Voices[v].PerfCurrent = FXParam;
					break;
				case 6: // Set Volume
					if (FXParam > 0x40)
					{
						if ((FXParam -= 0x50) >= 0)
						{
							if (FXParam <= 0x40) Voices[v].PerfSubVolume = FXParam;
							else if ((FXParam -= 0xa0 - 0x50) >= 0)
								if (FXParam <= 0x40)
									Voices[v].TrackMasterVolume = FXParam;
						}
					}
					else Voices[v].NoteMaxVolume = FXParam;

					break;
				case 7: // set speed
					Voices[v].PerfSpeed = Voices[v].PerfWait = FXParam;
					break;
			}
		}

		public void VoiceOnOff(int Voice, int OnOff)
		{
			if (Voice < 0 || Voice > 3) return;
			Voices[Voice].TrackOn = OnOff;
		}

		// AHXVoice ////////////////////////////////////////////////////////////////////////////////////////////////
		public class AHXVoice
		{
			// Read those variables for mixing!
			public int VoiceVolume, VoicePeriod;
			public sbyte[] VoiceBuffer = new sbyte[0x281]; // for oversampling optimization!

			public int Track, Transpose;
			public int NextTrack, NextTranspose;
			public int ADSRVolume; // fixed point 8:8
			public AHXEnvelope ADSR = new AHXEnvelope(); // frames/delta fixed 8:8
			public AHXInstrument Instrument; // current instrument
			public int InstrPeriod, TrackPeriod, VibratoPeriod;
			public int NoteMaxVolume, PerfSubVolume, TrackMasterVolume;
			public int NewWaveform, Waveform, PlantSquare, PlantPeriod, IgnoreSquare;
			public int TrackOn, FixedNote;
			public int VolumeSlideUp, VolumeSlideDown;
			public int HardCut, HardCutRelease, HardCutReleaseF;
			public int PeriodSlideSpeed, PeriodSlidePeriod, PeriodSlideLimit, PeriodSlideOn, PeriodSlideWithLimit;
			public int PeriodPerfSlideSpeed, PeriodPerfSlidePeriod, PeriodPerfSlideOn;
			public int VibratoDelay, VibratoCurrent, VibratoDepth, VibratoSpeed;
			public int SquareOn, SquareInit, SquareWait, SquareLowerLimit, SquareUpperLimit, SquarePos, SquareSign, SquareSlidingIn, SquareReverse;
			public int FilterOn, FilterInit, FilterWait, FilterLowerLimit, FilterUpperLimit, FilterPos, FilterSign, FilterSpeed, FilterSlidingIn, IgnoreFilter;
			public int PerfCurrent, PerfSpeed, PerfWait;
			public int WaveLength;
			public AHXPList PerfList;
			public int NoteDelayWait, NoteDelayOn, NoteCutWait, NoteCutOn;
			public sbyte[] AudioPointer;
			public Memory<sbyte> AudioSource;
			public int AudioPeriod, AudioVolume;
			public sbyte[] SquareTempBuffer = new sbyte[0x80];

			public AHXVoice()
			{
				Init();
			}

			public void Init()
			{
				//todo: just had new() called, so it should all be blank anyway
				//memset(this, 0, sizeof(AHXVoice));
				//memset(VoiceBuffer, 0, 0x281);
				TrackOn = 1;
				TrackMasterVolume = 0x40;
			}

			public void CalcADSR()
			{
				ADSR.aFrames = Instrument.Envelope.aFrames;
				ADSR.aVolume = Instrument.Envelope.aVolume * 256 / ADSR.aFrames;
				ADSR.dFrames = Instrument.Envelope.dFrames;
				ADSR.dVolume = (Instrument.Envelope.dVolume - Instrument.Envelope.aVolume) * 256 / ADSR.dFrames;
				ADSR.sFrames = Instrument.Envelope.sFrames;
				ADSR.rFrames = Instrument.Envelope.rFrames;
				ADSR.rVolume = (Instrument.Envelope.rVolume - Instrument.Envelope.dVolume) * 256 / ADSR.rFrames;
			}

		}

		// AHXWaves ////////////////////////////////////////////////////////////////////////////////////////////////
		public class AHXWaves
		{
			//public sbyte[] LowPasses = new sbyte[(0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31];
			//public sbyte[] Triangle04 = new sbyte[0x04], Triangle08 = new sbyte[0x08], Triangle10 = new sbyte[0x10], Triangle20 = new sbyte[0x20], Triangle40 = new sbyte[0x40], Triangle80 = new sbyte[0x80];
			//public sbyte[] Sawtooth04 = new sbyte[0x04], Sawtooth08 = new sbyte[0x08], Sawtooth10 = new sbyte[0x10], Sawtooth20 = new sbyte[0x20], Sawtooth40 = new sbyte[0x40], Sawtooth80 = new sbyte[0x80];
			//public sbyte[] Squares = new sbyte[0x80 * 0x20];
			//public sbyte[] WhiteNoiseBig = new sbyte[0x280 * 3];
			//public sbyte[] HighPasses = new sbyte[(0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31];

			public sbyte[] WaveBuffer = new sbyte[TotalSize];

			private const int offLowPasses = 0;
			private const int offTriangle04 = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31);
			private const int offTriangle08 = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4;
			private const int offTriangle10 = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8;
			private const int offTriangle20 = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16;
			private const int offTriangle40 = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16 + 32;
			private const int offTriangle80 = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16 + 32 + 64;
			private const int offSawtooth04 = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16 + 32 + 64 + 128;
			private const int offSawtooth08 = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16 + 32 + 64 + 128 + 4;
			private const int offSawtooth10 = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16 + 32 + 64 + 128 + 4 + 8;
			private const int offSawtooth20 = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16 + 32 + 64 + 128 + 4 + 8 + 16;
			private const int offSawtooth40 = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16 + 32 + 64 + 128 + 4 + 8 + 16 + 32;
			private const int offSawtooth80 = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16 + 32 + 64 + 128 + 4 + 8 + 16 + 32 + 64;
			public const int offSquares = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16 + 32 + 64 + 128 + 4 + 8 + 16 + 32 + 64 + 128;
			private const int offWhiteNoiseBig = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16 + 32 + 64 + 128 + 4 + 8 + 16 + 32 + 64 + 128 + (0x80 * 0x20);
			private const int offHighPasses = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16 + 32 + 64 + 128 + 4 + 8 + 16 + 32 + 64 + 128 + (0x80 * 0x20) + (0x280 * 3);
			private const int TotalSize = ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31) + 4 + 8 + 16 + 32 + 64 + 128 + 4 + 8 + 16 + 32 + 64 + 128 + (0x80 * 0x20) + (0x280 * 3) + ((0xfc + 0xfc + 0x80 * 0x1f + 0x80 + 3 * 0x280) * 31);

			public Memory<sbyte> LowPasses;
			public Memory<sbyte> Triangle04;
			public Memory<sbyte> Triangle08;
			public Memory<sbyte> Triangle10;
			public Memory<sbyte> Triangle20;
			public Memory<sbyte> Triangle40;
			public Memory<sbyte> Triangle80;
			public Memory<sbyte> Sawtooth04;
			public Memory<sbyte> Sawtooth08;
			public Memory<sbyte> Sawtooth10;
			public Memory<sbyte> Sawtooth20;
			public Memory<sbyte> Sawtooth40;
			public Memory<sbyte> Sawtooth80;
			public Memory<sbyte> Squares;
			public Memory<sbyte> WhiteNoiseBig;
			public Memory<sbyte> HighPasses;

			public AHXWaves()
			{
				LowPasses = new Memory<sbyte>(WaveBuffer, 0, offTriangle04);
				Triangle04 = new Memory<sbyte>(WaveBuffer, offTriangle04, offTriangle08 - offTriangle04);
				Triangle08 = new Memory<sbyte>(WaveBuffer, offTriangle08, offTriangle10 - offTriangle04);
				Triangle10 = new Memory<sbyte>(WaveBuffer, offTriangle10, offTriangle20 - offTriangle10);
				Triangle20 = new Memory<sbyte>(WaveBuffer, offTriangle20, offTriangle40 - offTriangle20);
				Triangle40 = new Memory<sbyte>(WaveBuffer, offTriangle40, offTriangle80 - offTriangle40);
				Triangle80 = new Memory<sbyte>(WaveBuffer, offTriangle80, offSawtooth04 - offTriangle80);
				Sawtooth04 = new Memory<sbyte>(WaveBuffer, offSawtooth04, offSawtooth08 - offSawtooth04);
				Sawtooth08 = new Memory<sbyte>(WaveBuffer, offSawtooth08, offSawtooth10 - offSawtooth08);
				Sawtooth10 = new Memory<sbyte>(WaveBuffer, offSawtooth10, offSawtooth20 - offSawtooth10);
				Sawtooth20 = new Memory<sbyte>(WaveBuffer, offSawtooth20, offSawtooth40 - offSawtooth20);
				Sawtooth40 = new Memory<sbyte>(WaveBuffer, offSawtooth40, offSawtooth80 - offSawtooth40);
				Sawtooth80 = new Memory<sbyte>(WaveBuffer, offSawtooth80, offSquares - offSawtooth80);
				Squares = new Memory<sbyte>(WaveBuffer, offSquares, offWhiteNoiseBig - offSquares);
				WhiteNoiseBig = new Memory<sbyte>(WaveBuffer, offWhiteNoiseBig, offHighPasses - offWhiteNoiseBig);
				HighPasses = new Memory<sbyte>(WaveBuffer, offHighPasses, WaveBuffer.Length - offHighPasses);

				//not strictly necessary, it will work itself out, however these spans will be the expected size and not all of the remaining array.
				waveCache.Add(0, LowPasses);
				waveCache.Add(offTriangle04, Triangle04);
				waveCache.Add(offTriangle08, Triangle08);
				waveCache.Add(offTriangle10, Triangle10);
				waveCache.Add(offTriangle20, Triangle20);
				waveCache.Add(offTriangle40, Triangle40);
				waveCache.Add(offTriangle80, Triangle80);
				waveCache.Add(offSawtooth04, Sawtooth04);
				waveCache.Add(offSawtooth08, Sawtooth08);
				waveCache.Add(offSawtooth10, Sawtooth10);
				waveCache.Add(offSawtooth20, Sawtooth20);
				waveCache.Add(offSawtooth40, Sawtooth40);
				waveCache.Add(offSawtooth80, Sawtooth80);
				waveCache.Add(offSquares, Squares);
				waveCache.Add(offWhiteNoiseBig, WhiteNoiseBig);
				waveCache.Add(offHighPasses, HighPasses);

				Generate();
			}

			private void Generate()
			{
				GenerateSawtooth(Sawtooth04.Span, 0x04);
				GenerateSawtooth(Sawtooth08.Span, 0x08);
				GenerateSawtooth(Sawtooth10.Span, 0x10);
				GenerateSawtooth(Sawtooth20.Span, 0x20);
				GenerateSawtooth(Sawtooth40.Span, 0x40);
				GenerateSawtooth(Sawtooth80.Span, 0x80);
				GenerateTriangle(Triangle04.Span, 0x04);
				GenerateTriangle(Triangle08.Span, 0x08);
				GenerateTriangle(Triangle10.Span, 0x10);
				GenerateTriangle(Triangle20.Span, 0x20);
				GenerateTriangle(Triangle40.Span, 0x40);
				GenerateTriangle(Triangle80.Span, 0x80);
				GenerateSquare(Squares.Span);
				GenerateWhiteNoise(WhiteNoiseBig.Span, 0x280 * 3);
				GenerateFilterWaveforms(new Span<sbyte>(WaveBuffer, offTriangle04, offHighPasses - offTriangle04), LowPasses.Span, HighPasses.Span);
			}

			public int WaveToOffset(Memory<sbyte> wave)
			{
				if (wave.Equals(LowPasses)) return offLowPasses;
				if (wave.Equals(Triangle04)) return offTriangle04;
				if (wave.Equals(Triangle08)) return offTriangle08;
				if (wave.Equals(Triangle10)) return offTriangle10;
				if (wave.Equals(Triangle20)) return offTriangle20;
				if (wave.Equals(Triangle40)) return offTriangle40;
				if (wave.Equals(Triangle80)) return offTriangle80;
				if (wave.Equals(Sawtooth04)) return offSawtooth04;
				if (wave.Equals(Sawtooth08)) return offSawtooth08;
				if (wave.Equals(Sawtooth10)) return offSawtooth10;
				if (wave.Equals(Sawtooth20)) return offSawtooth20;
				if (wave.Equals(Sawtooth40)) return offSawtooth40;
				if (wave.Equals(Sawtooth80)) return offSawtooth80;
				if (wave.Equals(Squares)) return offSquares;
				if (wave.Equals(WhiteNoiseBig)) return offWhiteNoiseBig;
				if (wave.Equals(HighPasses)) return offHighPasses;
				throw new ArgumentOutOfRangeException();
			}

			private readonly Dictionary<int, Memory<sbyte>> waveCache = new Dictionary<int, Memory<sbyte>>();

			public Memory<sbyte> OffsetToWave(int offset)
			{
				if (waveCache.TryGetValue(offset, out var wave))
					return wave;
				return waveCache[offset] = new Memory<sbyte>(WaveBuffer, offset, WaveBuffer.Length - offset);
			}

			private static void clip(ref float x)
			{
				if (x > 127.0f)
				{
					x = 127.0f;
					return;
				}

				if (x < -128.0f)
				{
					x = -128.0f;
					return;
				}
			}

			private void GenerateFilterWaveforms(Span<sbyte> Buffer, Span<sbyte> Low, Span<sbyte> High)
			{
				int[] LengthTable =
				{
					3, 7, 0xf, 0x1f, 0x3f, 0x7f, 3, 7, 0xf, 0x1f, 0x3f, 0x7f,
					0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f,
					0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f,
					(0x280 * 3) - 1
				};

				int lowHigh = 0;
				for (int temp = 0, freq = 8; temp < 31; temp++, freq += 3)
				{
					int a0 = 0;
					for (int waves = 0; waves < 6 + 6 + 0x20 + 1; waves++)
					{
						float fre = (float)freq * 1.25f / 100.0f;
						float high, mid = 0.0f, low = 0.0f;
						for (int i = 0; i <= LengthTable[waves]; i++)
						{
							high = Buffer[a0 + i] - mid - low;
							clip(ref high);
							mid += high * fre;
							clip(ref mid);
							low += mid * fre;
							clip(ref low);
						}

						for (int i = 0; i <= LengthTable[waves]; i++)
						{
							high = Buffer[a0 + i] - mid - low;
							clip(ref high);
							mid += high * fre;
							clip(ref mid);
							low += mid * fre;
							clip(ref low);
							Low[lowHigh] = (sbyte)low;
							High[lowHigh] = (sbyte)high;
							lowHigh++;
						}

						a0 += LengthTable[waves] + 1;
					}
				}
			}

			private void GenerateTriangle(Span<sbyte> Buffer, int Len)
			{
				int d2 = Len;
				int d5 = d2 >> 2;
				int d1 = 128 / d5;
				int d4 = -(d2 >> 1);
				//sbyte* edi = Buffer;
				int edi = 0;
				int eax = 0;
				for (int ecx = 0; ecx < d5; ecx++)
				{
					Buffer[edi++] = (sbyte)eax;
					eax += d1;
				}

				Buffer[edi++] = 0x7f;
				if (d5 != 1)
				{
					eax = 128;
					for (int ecx = 0; ecx < d5 - 1; ecx++)
					{
						eax -= d1;
						Buffer[edi++] = (sbyte)eax;
					}
				}

				int esi = edi + d4;
				for (int ecx = 0; ecx < d5 * 2; ecx++)
				{
					Buffer[edi++] = Buffer[esi++];
					if (Buffer[edi - 1] == 0x7f) Buffer[edi - 1] = -128;
					else Buffer[edi - 1] = (sbyte)-Buffer[edi - 1];
				}
			}

			private void GenerateSquare(Span<sbyte> Buffer)
			{
				int edi = 0;
				for (int ebx = 1; ebx <= 0x20; ebx++)
				{
					for (int ecx = 0; ecx < (0x40 - ebx) * 2; ecx++) Buffer[edi++] = -128;
					for (int ecx = 0; ecx < ebx * 2; ecx++) Buffer[edi++] = 0x7f;
				}
			}

			private void GenerateSawtooth(Span<sbyte> Buffer, int Len)
			{
				int edi = 0;
				int ebx = 256 / (Len - 1), eax = -128;
				for (int ecx = 0; ecx < Len; ecx++)
				{
					Buffer[edi++] = (sbyte)eax;
					eax += ebx;
				}
			}

			private void GenerateWhiteNoise(Span<sbyte> Buffer, int Len)
			{
				//	__asm {
				//		mov edi, Buffer
				//		mov ecx, Len
				//		mov eax, 0x41595321 // AYS!
				//loop0:	test eax, 0x100
				//		je lower
				//		cmp ax, 0
				//		jl mi
				//		mov byte ptr [edi], 0x7f
				//		jmp weida
				//mi:		mov byte ptr [edi], 0x80
				//		jmp weida
				//lower:	mov byte ptr [edi], al
				//weida:	inc edi
				//		ror eax, 5
				//		xor al, 10011010b
				//		mov bx, ax
				//		rol eax, 2
				//		add bx, ax
				//		xor ax, bx
				//		ror eax, 3
				//		dec ecx
				//		jnz loop0
				//	}

				int eax = 0x41595321;
				ushort bx;
				int bptr = 0;
				while (Len-- != 0)
				{
					if ((eax & 0x100) != 0)
					{
						if ((short)eax >= 0)
							Buffer[bptr++] = 0x7f;
						else
							Buffer[bptr++] = -128;
					}
					else
					{
						Buffer[bptr++] = (sbyte)eax;
					}

					eax = (eax >> 5) | (eax << 27);
					eax = eax ^ (128 + 16 + 8 + 2);
					bx = (ushort)eax;
					eax = (eax << 2) | (eax >> 30);
					bx += (ushort)eax;
					eax ^= bx;
					eax = (eax >> 3) | (eax << 29);
				}
			}
		}
	}

	// AHXOutput ///////////////////////////////////////////////////////////////////////////////////////////////
	public class AHXOutput
	{
		public const int AHXOF_BOOST = 0;
		public const int AHXOI_OVERSAMPLING = 1;

		public float Period2Freq(float period) { return 3579545.25f / period; }

		public int Bits, Frequency, MixLen;
		public int Hz;
		public int Playing, Paused;

		public AHXPlayer Player;

		//Options
		public int Oversampling;
		public float Boost;

		public int[] MixingBuffer;
		public int[][] VolumeTable = new int[65][];

		public AHXOutput()
		{
			for (int i = 0; i < 65; i++)
				VolumeTable[i] = new int[256];
			Player = null;
			MixingBuffer = null;
			Playing = Paused = 0;
		}

		protected int Init(int Frequency, int Bits, int MixLen, float Boost, int Hz)
		{
			this.MixLen = MixLen;
			this.Frequency = Frequency;
			this.Bits = Bits;
			this.Hz = Hz;
			this.MixingBuffer = new int[MixLen * Frequency / Hz];
			return SetOption(AHXOF_BOOST, Boost);
		}

		private int Free()
		{
			MixingBuffer = null;
			return 1;
		}

		public int SetOption(int Option, int Value)
		{
			switch (Option)
			{
				case AHXOI_OVERSAMPLING: Oversampling = Value; return 1;
				default: return 0;
			}
		}

		public int SetOption(int Option, float Value)
		{
			switch (Option)
			{
				case AHXOF_BOOST:
					{
						// Initialize volume table
						for (int i = 0; i < 65; i++)
							for (int j = -128; j < 128; j++)
								VolumeTable[i][j + 128] = (int)(i * j * Value) / 64;
						Boost = Value;
					}
					return 1;
				default: return 0;
			}
		}

		public int GetOption(int Option, ref int pValue)
		{
			switch (Option)
			{
				case AHXOI_OVERSAMPLING: pValue = Oversampling; return 1;
				default: return 0;
			}
		}

		public int GetOption(int Option, ref float pValue)
		{
			switch (Option)
			{
				case AHXOF_BOOST: pValue = Boost; return 1;
				default: return 0;
			}
		}

		//#define min(a,b)  (((a) < (b)) ? (a) : (b))

		static int[] pos = { 0, 0, 0, 0 };
		private void MixChunk(int NrSamples, ref int mb)
		{
			for (int v = 0; v < 4; v++)
			{
				if (Player.Voices[v].VoiceVolume == 0) continue;
				float freq = Period2Freq(Player.Voices[v].VoicePeriod);
				int delta = (int)(freq * (1 << 16) / Frequency);
				int samples_to_mix = NrSamples;
				int mixpos = 0;
				while (samples_to_mix != 0)
				{
					if (pos[v] > (0x280 << 16)) pos[v] -= 0x280 << 16;
					int thiscount = Math.Min(samples_to_mix, ((0x280 << 16) - pos[v] - 1) / delta + 1);
					samples_to_mix -= thiscount;
					int[] VolTab = VolumeTable[Player.Voices[v].VoiceVolume];
					//INNER LOOP
					if (Oversampling != 0)
					{
						for (int i = 0; i < thiscount; i++)
						{
							int offset = pos[v] >> 16;
							int sample1 = VolTab[Player.Voices[v].VoiceBuffer[offset] + 128];
							int sample2 = VolTab[Player.Voices[v].VoiceBuffer[offset + 1] + 128];
							int frac1 = pos[v] & ((1 << 16) - 1);
							int frac2 = (1 << 16) - frac1;
							MixingBuffer[mb + mixpos++] += ((sample1 * frac2) + (sample2 * frac1)) >> 16;
							pos[v] += delta;
						}
					}
					else
					{
						for (int i = 0; i < thiscount; i++)
						{
							MixingBuffer[mb + mixpos++] += VolTab[Player.Voices[v].VoiceBuffer[pos[v] >> 16] + 128];
							pos[v] += delta;
						}
					}
				} // while
			} // v = 0-3
			mb += NrSamples;
		}

		protected void MixBuffer()
		{
			int NrSamples = Frequency / Hz / Player.Song.SpeedMultiplier;
			//int[] mb = MixingBuffer;
			int mb = 0;

			//memset(MixingBuffer, 0, MixLen * Frequency / Hz * sizeof(int));
			Array.Clear(MixingBuffer, 0, MixLen * Frequency / Hz);
			for (int f = 0; f < MixLen * Player.Song.SpeedMultiplier /* MixLen = # frames */; f++)
			{
				Player.PlayIRQ();
				MixChunk(NrSamples, ref mb);
			} // frames
		}
	}
}
