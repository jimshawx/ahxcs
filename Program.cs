﻿using System;
using System.Threading;
using AHX;

namespace AHXcs
{
	class Program
	{
		private static AHXPlayer player;
		private static IAHXOutput output;

		private static bool tune_init(string fname)
		{
			player = new AHXPlayer();
			if (player != null)
			{
				player.Init(null);
				if (player.LoadSong(fname) != 0)
				{
					player.InitSubsong(0);

					for (int i = 0; i < 25; i++)
						player.NextPosition();

					//output = new AHXWaveOut();
					output = new AHXXAudio2();
					output.Init();
					output.Play(player);
					return true;
				}
				else
				{
					//delete player;
					player.Dispose();
					player = null;
					output = null;
					return false;
				}
			}

			return false;
		}

		private static void tune_shutdown()
		{
			if (output != null)
			{
				output.StopBackgroundPlay();
				//delete output;
				output.Dispose();
				output = null;
			}

			if (player != null)
			{
				//delete player;
				player.Dispose();
				player = null;
			}
		}

		private static void tune_start()
		{
			output.StartBackgroundPlay();
		}

		static void Main(string[] args)
		{
			tune_init("m0d_-_omc_meat_pie.ahx");
			tune_start();

			for (;;)
				Thread.Sleep(5000);

			tune_shutdown();
		}
	}
}
