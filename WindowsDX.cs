using System;
using System.Linq;
using System.Runtime.InteropServices;
using AHXcs;
using SharpDX;
using SharpDX.Multimedia;
using SharpDX.XAudio2;

namespace AHX
{
	public class AHXXAudio2 : AHXOutput, IAHXOutput
	{
		private volatile int StopPlaying;
		private int NrBlocks, Frames, BlockLen;

		[Flags]
		private enum XAUDIO2_LOG
		{
			ERRORS = 0x0001, // For handled errors with serious effects.
			WARNINGS = 0x0002, // For handled errors that may be recoverable.
			INFO = 0x0004, // Informational chit-chat (e.g. state changes).
			DETAIL = 0x0008, // More detailed chit-chat.
			API_CALLS = 0x0010, // Public API function entries and exits.
			FUNC_CALLS = 0x0020, // Internal function entries and exits.
			TIMING = 0x0040, // Delays detected and other timing data.
			LOCKS = 0x0080, // Usage of critical sections and mutexes.
			MEMORY = 0x0100, // Memory heap usage information.
			STREAMING = 0x1000, // Audio streaming information.
		}

		private readonly XAudio2 xaudio;
		private readonly MasteringVoice masteringVoice;

		private SourceVoice xaudioVoice;

		private class Block
		{
			public AudioBuffer xaudioBuffer;
			public bool Active;
		}

		private Block[] blocks;

		public AHXXAudio2()
		{
			xaudio = new XAudio2();

			xaudio.SetDebugConfiguration(new DebugConfiguration
			{
				TraceMask = (int)(XAUDIO2_LOG.ERRORS | XAUDIO2_LOG.WARNINGS | XAUDIO2_LOG.DETAIL | XAUDIO2_LOG.API_CALLS | XAUDIO2_LOG.FUNC_CALLS),
				BreakMask = 0,
				LogThreadID = true,
				LogFileline = true,
				LogFunctionName = true,
				LogTiming = true
			}, IntPtr.Zero);

			masteringVoice = new MasteringVoice(xaudio);
			masteringVoice.GetVoiceDetails(out VoiceDetails _);
		}

		public void Dispose()
		{
			Free();
		}

		public int Init(int Frequency = 44100, int Bits = 16, int Frames = 2, int NrBlocks = 16, float Boost = 1.0f, int Hz = 50)
		{
			this.Frames = Frames;
			this.NrBlocks = NrBlocks;

			if (base.Init(Frequency, Bits, Frames, Boost, Hz) == 0) return 0;

			BlockLen = Frequency * Bits / 8 * Frames / Hz;
			//44100 * 16 / 8 * 2 /50 = 3528

			var wv = new WaveFormat(Frequency, Bits, 1);
			
			xaudioVoice = new SourceVoice(xaudio, wv, VoiceFlags.None);

			blocks = new Block[NrBlocks];
			for (int i = 0; i < NrBlocks; i++)
			{
				blocks[i] = new Block
				{
					xaudioBuffer = new AudioBuffer {AudioBytes = BlockLen, AudioDataPointer = Utilities.AllocateMemory(BlockLen), PlayLength = BlockLen / (wv.BitsPerSample / 8), Context = (IntPtr)i}
				};
			}

			xaudioVoice.BufferEnd += ptr => { blocks[ptr.ToInt32()].Active = false; PlayIt(); };

			return 1;
		}

		private int Free()
		{
			for (int i = 0; i < NrBlocks; i++)
				Utilities.FreeMemory(blocks[i].xaudioBuffer.AudioDataPointer);

			xaudioVoice.Stop();
			xaudioVoice.Dispose();

			masteringVoice.DestroyVoice();
			masteringVoice.Dispose();
			xaudio.Dispose();

			return 1;
		}

		public int StartBackgroundPlay()
		{
			StopPlaying = 0;

			//prime the playback buffer(s)
			for (int i = 0; i < NrBlocks; i++)
				PlayIt();

			//start the audio
			xaudioVoice.Start();

			return 1;
		}

		public int StopBackgroundPlay()
		{
			StopPlaying = 1;
			//playback buffers will drain naturally
			return 1;
		}

		public int Play(AHXPlayer Player)
		{
			if (Stop() == 0 || Player == null) return 0;
			this.Player = Player;
			Playing = 1;
			return 1;
		}

		public int Pause()
		{
			Paused = 1;
			xaudioVoice.Stop();
			return 1;
		}

		public int Resume()
		{
			Paused = 0;
			xaudioVoice.Start();
			return 1;
		}

		public int Stop()
		{
			Playing = 0;
			xaudioVoice.Stop();
			return 1;
		}

		public int SetVolume(int Volume)
		{
			float v = Volume / 64.0f;
			masteringVoice.SetChannelVolumes(2, new [] {v,v});
			return 1;
		}

		private void PlayIt()
		{
			if (StopPlaying != 0) return;

			// mix a new block (size: Frames frames = BlockLen)
			MixBuffer();

			// prepare it for waveOut
			OutputBuffer();
		}

		private void OutputBuffer()
		{
			const int LOW_CLIP16 = -0x8000;
			const int HI_CLIP16 = 0x7FFF;
			const int LOW_CLIP8 = -0x80;
			const int HI_CLIP8 = 0x7F;

			int thissample;

			var x = blocks.First(x => !x.Active);
			x.Active = true;

			if (Bits == 16)
			{
				short[] lpChunkDataW = new short[BlockLen / (Bits / 8)];
				for (int s = 0; s < BlockLen / (Bits / 8); s++)
				{
					thissample = MixingBuffer[s] << 6; // 16 bit
					lpChunkDataW[s] = (short)(thissample < LOW_CLIP16 ? LOW_CLIP16 : thissample > HI_CLIP16 ? HI_CLIP16 : thissample);
				}
				Marshal.Copy(lpChunkDataW, 0, x.xaudioBuffer.AudioDataPointer, lpChunkDataW.Length);
			}
			else if (Bits == 8)
			{
				byte[] lpChunkDataB = new byte[BlockLen / (Bits / 8)];
				for (int s = 0; s < BlockLen / (Bits / 8); s++)
				{
					thissample = MixingBuffer[s] >> 2; // 8 bit
					lpChunkDataB[s] = (byte)((thissample < LOW_CLIP8 ? LOW_CLIP8 : thissample > HI_CLIP8 ? HI_CLIP8 : thissample) + 128);
				}
				Marshal.Copy(lpChunkDataB, 0, x.xaudioBuffer.AudioDataPointer, lpChunkDataB.Length);
			}
			xaudioVoice.SubmitSourceBuffer(x.xaudioBuffer, null);
		}
	}
}