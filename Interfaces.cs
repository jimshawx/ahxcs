﻿using System;
using AHX;

namespace AHXcs
{
	public interface IAHXOutput : IDisposable
	{
		int Init(int Frequency = 44100, int Bits = 16, int Frames = 2, int NrBlocks = 16, float Boost = 1.0f, int Hz = 50);
		int StartBackgroundPlay();
		int StopBackgroundPlay();
		int Play(AHXPlayer Player);
		int Pause();
		int Resume();
		int Stop();
		int SetVolume(int Volume);
	}
}