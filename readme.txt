Copyright:
--------
WinAHX and the AHX Replayer, although copyrighted
by Abyss, are Freeware! You may spread them as you
like - as long as you don't charge any money for it!

Code
--------
Bartman

Quality Assurance
--------
Pink

Original Amiga Code
--------
Dexter

Original Concept
--------
Pink

License
--------
do whatever you like with the source codes, BUT:
- DON'T sell them
- DON'T claim they're yours.
- DON'T use commercially.
- bla..bla..


Notes
--------
compiles with Visual C++, if you use others, please mind that "char = signed char" for this source.

contact me for anything EXCEPT support for the sources: barto@gmx.net

Bartman/Abyss
19/09/2000
new Abyss homepage: www.abyss-online.de